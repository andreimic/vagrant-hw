# Vagrant hello world

Hello from the other side OMMAGAWD

## Pre-requisites

1. [Download and install Vagrant](https://www.vagrantup.com/downloads.html)
1. [Download and install VirtualBox](https://www.virtualbox.org/)

## Running it

```
$ vagrant up
$ vagrant ssh
```